package claysoftener.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.Bank;
import org.powerbot.script.rt4.ClientContext;

import claysoftener.ClaySoftener;
import claysoftener.tasks.Task;

public class WithdrawClay extends Task<ClientContext> {
	
	private boolean timedOut = false;
	
	public WithdrawClay(ClientContext ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean activate() {
		// active if bank booth in viewport & only buckets in inventory
		return (ctx.objects.select().id(ClaySoftener.BOOTH_ID).nearest().poll().inViewport() || ctx.bank.opened())
			&& ctx.inventory.select().count() == 14;
	}

	@Override
	public void execute() {
		timedOut = false;
		
		// if bank isn't open, open it
		if (!ctx.bank.opened()) {
			ctx.objects.select().id(ClaySoftener.BOOTH_ID).nearest().poll().interact("Bank", "Bank booth");
			
			// continue once bank open
			Condition.wait(new Callable<Boolean>() {				
				@Override
				public Boolean call() {
					timedOut = !ctx.bank.opened();
					return !timedOut;
				}
			}, 200, 20);
		} else {
			// else get the clay
			ctx.bank.withdraw(ClaySoftener.CLAY_ID, Bank.Amount.ALL);
			
			if (ctx.bank.select().id(ClaySoftener.CLAY_ID).count() == 0) {
				if (ctx.inventory.select().id(ClaySoftener.CLAY_ID).count() == 0) {
					// no more clay, end script
					ctx.controller.stop();
				}
			}
			
			// contine once clay is in inventory
			Condition.wait(new Callable<Boolean>() {				
				@Override
				public Boolean call() {
					return !ctx.inventory.select().id(ClaySoftener.CLAY_ID).isEmpty();
				}
			}, 200, 20);
		}
		
		if(timedOut) {
			// might be stuck
			ctx.movement.step(ClaySoftener.BANK_AREA.getRandomTile());
		}
		
	}

}
