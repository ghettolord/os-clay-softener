package claysoftener.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.Component;

import claysoftener.ClaySoftener;

public class SoftenClay extends Task<ClientContext> {

	public SoftenClay(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		// if we have clay & filled buckets
		return !ctx.inventory.select().id(ClaySoftener.CLAY_ID).isEmpty()
			&& !ctx.inventory.select().id(ClaySoftener.FILLED_BUCKET_ID).isEmpty();
	}

	@Override
	public void execute() {
		final Component soften = ctx.widgets.component(ClaySoftener.WIDGET, ClaySoftener.COMPONENT);
		
		if(ctx.bank.opened()) {
			ctx.bank.close();
		}
		
		if(soften.visible()) {
			soften.interact("Make all");
			
			// continue once all clay processed
			Condition.wait(new Callable<Boolean>() {				
				@Override
				public Boolean call() {
					return ctx.inventory.select().id(ClaySoftener.FILLED_BUCKET_ID).isEmpty()
						|| ctx.inventory.select().id(ClaySoftener.CLAY_ID).isEmpty();
				}
			}, 200, 80);
		} else {
			ctx.inventory.select().id(ClaySoftener.FILLED_BUCKET_ID).poll().interact("Use", "Bucket of water");
			Condition.sleep(400);
			ctx.inventory.select().id(ClaySoftener.CLAY_ID).poll().interact("Use");
			Condition.wait(new Callable<Boolean>() {				
				@Override
				public Boolean call() {
					return soften.visible();
				}
			}, 200, 10);
		}
	}

}
