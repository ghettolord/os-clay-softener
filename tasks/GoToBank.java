package claysoftener.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.ClientContext;

import claysoftener.ClaySoftener;

public class GoToBank extends Task<ClientContext> {

	public GoToBank(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		// if bank not in viewport & no clay in inventory
		return !ctx.objects.select().id(ClaySoftener.BOOTH_ID).nearest().poll().inViewport()
			&& ctx.inventory.select().id(ClaySoftener.CLAY_ID).isEmpty();
	}

	@Override
	public void execute() {
		ctx.movement.step(ClaySoftener.BANK_AREA.getClosestTo(ctx.players.local()));
		
		// continue once bank is in viewport
		Condition.wait(new Callable<Boolean>() {				
			@Override
			public Boolean call() {
				return ctx.objects.select().id(ClaySoftener.BOOTH_ID).nearest().poll().inViewport();
			}
		}, 200, 5);
	}

}
