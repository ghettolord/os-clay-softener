package claysoftener.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.ClientContext;

import claysoftener.ClaySoftener;

public class FillBuckets extends Task<ClientContext> {

	public FillBuckets(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		// if at fountain & have empty buckets
		return ctx.objects.select().id(ClaySoftener.FOUNTAIN_ID).nearest().poll().inViewport()
			&& !ctx.inventory.select().id(ClaySoftener.EMPTY_BUCKET_ID).isEmpty();
	}

	@Override
	public void execute() {
		ctx.inventory.select().id(ClaySoftener.EMPTY_BUCKET_ID).poll().interact("Use", "Bucket");
		ctx.objects.select().id(ClaySoftener.FOUNTAIN_ID).nearest().poll().interact("Use", "Fountain");
		
		// continue once all buckets filled
		Condition.wait(new Callable<Boolean>() {				
			@Override
			public Boolean call() {
				return ctx.inventory.select().id(ClaySoftener.EMPTY_BUCKET_ID).isEmpty();
			}
		}, 200, 40);
	}

}
