package claysoftener.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.ClientContext;

import claysoftener.ClaySoftener;

public class GoToFountain extends Task<ClientContext> {

	public GoToFountain(ClientContext ctx) {
		super(ctx);
	}

	@Override
	public boolean activate() {
		// activate if clay & 14 empty buckets in inventory, but not at fountain
		return !ctx.inventory.select().id(ClaySoftener.CLAY_ID).isEmpty()
			&& (ctx.inventory.select().id(ClaySoftener.EMPTY_BUCKET_ID).count() == 14)
			&& !ctx.objects.select().id(ClaySoftener.FOUNTAIN_ID).nearest().poll().inViewport();
	}

	@Override
	public void execute() {
		ctx.movement.step(ClaySoftener.FOUNTAIN_AREA.getClosestTo(ctx.players.local()));
		
		// continue once fountain is in viewport
		Condition.wait(new Callable<Boolean>() {				
			@Override
			public Boolean call() {
				return ctx.objects.select().id(ClaySoftener.FOUNTAIN_ID).nearest().poll().inViewport();
			}
		}, 200, 5);
	}

}
