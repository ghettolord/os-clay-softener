package claysoftener.tasks;

import java.util.concurrent.Callable;

import org.powerbot.script.Condition;
import org.powerbot.script.rt4.Bank;
import org.powerbot.script.rt4.ClientContext;
import org.powerbot.script.rt4.Game;

import claysoftener.ClaySoftener;

public class DepositClay extends Task<ClientContext> {
	
	private boolean timedOut = false;
	
	public DepositClay(ClientContext ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean activate() {
		// active if bank booth in viewport & only buckets & soft clay in inventory
		return (ctx.objects.select().id(ClaySoftener.BOOTH_ID).nearest().poll().inViewport() || ctx.bank.opened())
			&& ctx.inventory.select().id(ClaySoftener.CLAY_ID).isEmpty()
			&& !ctx.inventory.select().id(ClaySoftener.SOFT_CLAY_ID).isEmpty();
	}

	@Override
	public void execute() {
		timedOut = false;
		
		// if bank isn't open, open it
		if (!ctx.bank.opened()) {
			ctx.objects.select().id(ClaySoftener.BOOTH_ID).nearest().poll().interact("Bank", "Bank booth");
			
			// continue once bank open
			Condition.wait(new Callable<Boolean>() {				
				@Override
				public Boolean call() {
					timedOut = !ctx.bank.opened();
					return !timedOut;
				}
			}, 200, 20);
		} else {
			// else bank the clay
			ctx.bank.deposit(ClaySoftener.SOFT_CLAY_ID, Bank.Amount.ALL);
			
			// continue once only 14 items in inventory
			Condition.wait(new Callable<Boolean>() {				
				@Override
				public Boolean call() {
					return ctx.inventory.select().count() == 14;
				}
			}, 200, 20);
		}
		
		if(timedOut) {
			// might be stuck
			ctx.movement.step(ClaySoftener.BANK_AREA.getRandomTile());
		}
		
		
	}

}
