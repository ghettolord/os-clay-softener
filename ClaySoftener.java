package claysoftener;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.powerbot.script.Area;
import org.powerbot.script.MessageEvent;
import org.powerbot.script.MessageListener;
import org.powerbot.script.PaintListener;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Script;
import org.powerbot.script.Tile;
import org.powerbot.script.rt4.ClientContext;

import claysoftener.tasks.DepositClay;
import claysoftener.tasks.FillBuckets;
import claysoftener.tasks.GoToBank;
import claysoftener.tasks.GoToFountain;
import claysoftener.tasks.SoftenClay;
import claysoftener.tasks.Task;
import claysoftener.tasks.WithdrawClay;

@Script.Manifest(name ="OS Clay Softener", description = "varrock east-bank clay softener", properties = "game=4;")
public class ClaySoftener extends PollingScript<ClientContext> implements PaintListener, MessageListener {
	// Constants
	public static final int WIDGET = 309;
	public static final int COMPONENT = 6;
	
	public static final int BOOTH_ID = 11748;
	public static final int FOUNTAIN_ID = 2644;
	public static final int CLAY_ID = 434;
	public static final int SOFT_CLAY_ID = 1761;
	public static final int EMPTY_BUCKET_ID = 1925;
	public static final int FILLED_BUCKET_ID = 1929;
	
	public static final Area BANK_AREA = new Area(new Tile(3254, 3420, 0), new Tile(3251, 3422, 0));
	public static final Area FOUNTAIN_AREA = new Area(new Tile(3237, 3433, 0), new Tile(3240, 3436, 0));
	
	public static final Font ARIAL = new Font("Arial", Font.BOLD, 12);
	
	// Variables
	private int profitPerClay = 0;
	private int claySoftened = 0;
	
	private List<Task> taskList = new ArrayList<Task>();
	
	@Override
	public void start() {
		// softClayPrice - clayPrice, should get non hard prices here..
		profitPerClay = 119 - 85;
		
		taskList.addAll(Arrays.asList(new SoftenClay(ctx), new FillBuckets(ctx),
			new WithdrawClay(ctx), new DepositClay(ctx), new GoToBank(ctx), new GoToFountain(ctx)));
	}
	
	@Override
	public void poll() {
		// enable run
		if (ctx.movement.energyLevel() > 40) {
			ctx.movement.running(true);
		}
		
		for (Task task : taskList) {
			if (task.activate()) {
				task.execute();
				break;
			}
		}
	}
	
	@Override
	public void repaint(Graphics graphics) {
		final Graphics2D g = (Graphics2D) graphics;
		g.setFont(ARIAL);
		
		final long currentRuntime = getRuntime();
		final long second = (currentRuntime / 1000) % 60;
		final long minute = (currentRuntime / (1000 * 60)) % 60;
		final long hour = (currentRuntime / (1000 * 60 * 60)) % 24;
		final String duration = String.format("Duration: %02d:%02d:%02d", hour, minute, second);
		
		final int profit = profitPerClay * claySoftened;
		final int profitHr = (int) ((profit * 3600000D) / getRuntime());
		final int clayHr = (int) ((claySoftened * 3600000D) / getRuntime());
		
		g.setColor(Color.BLACK);
		g.fillRect(5, 5, 200, 60);
		
		g.setColor(Color.WHITE);
		g.drawString(duration, 10, 20);
		g.drawString(String.format("Clay: %,d (%,d/hr)", claySoftened, clayHr), 10, 40);
		g.drawString(String.format("Profit: %,d (%,d/hr)", profit, profitHr), 10, 60);
	}

	@Override
	public void messaged(MessageEvent e) {
		final String msg = e.text().toLowerCase();
		if(msg.equals("you now have some soft workable clay.")) {
			claySoftened++;
		}
	}

}
